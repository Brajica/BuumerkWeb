import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreComponent } from './store.component'
import {BuuappsComponent} from './buuapps/buuapps.component';
import {CategoriesComponent} from './categories/categories.component';
const routes: Routes = [
  {path: '', component: StoreComponent, children: [
      {path: '', redirectTo: 'buuapps', pathMatch: 'full'},
      {path: 'buuapps', component: BuuappsComponent},
      {path: 'categories', component: CategoriesComponent}
    ]}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule {

}
